import json
import os
import getopt
import sys

object_repo = "Object Repository\\"
keywords = "Keywords\\"

def read(file):
    with open(file, 'r') as file:
        data = json.load(file)
        return data


def search_strings_in_files(root_folder, target_strings):
    matching_files = []

    for root, dirs, files in os.walk(root_folder):
        for file_name in files:
            file_path = os.path.join(root, file_name)
            try:
                with open(file_path, 'r', encoding='utf-8') as file:
                    file_contents = file.read()
                    all_strings_present = all(target_string in file_contents for target_string in target_strings)
                    if all_strings_present:
                        matching_files.append(file_path)
            except Exception as e:
                print(f"Error reading file {file_path}: {e}")

    return matching_files



def main(argv):
    swagger_file = None
    katalon_source = None

    try:
        opts, args = getopt.getopt(argv, "l:s:", ["location=", "swagger="])
    except getopt.GetoptError:
        print("Usage: ./coverage.py -l <location> -s <file>")
        sys.exit(1)

    for opt, arg in opts:
        if opt in ("-s", "--swagger"):
            swagger_file = arg
        elif opt in ("-l", "--location"):
            katalon_source = arg

    if swagger_file is None:
        print("Swagger argument is required. Usage: ./coverage.py --swagger <file>")
        sys.exit(1)

    swagger = read(swagger_file)
    paths = []

    # Retrieve paths from swagger JSON
    for path in swagger['paths']:
        paths.append(path)

    paths = sorted(paths)

    cnt_paths = len(paths)
    cnt_object = 0
    cnt_keyword = 0

    full_path = katalon_source+object_repo

    print (f"Paths found:{cnt_paths}")

    # Check if path has a matching Object Repository file.
    # Naive matching: only checks precense of path string in object repository files
    for path in paths:
        methods = list(swagger['paths'][path].keys())
        methods = [item.upper() for item in methods]
        cnt_paths += len(methods)-1

        # print(f">> {path}/{methods}")
        for method in methods:
            files = search_strings_in_files(full_path, [path.replace("{","${"), f"<restRequestMethod>{method}</restRequestMethod>"])

            if not files:
                print(f"\tNo object\t: {method[:min(len(method), 4)]}\t{path}")
            else:
                cnt_object += 1
                has_keyword = False

                # If object file exists, check if it is actually used:
                # Naive matching: only checks precense of object name in keyword files
                for file in files:
                    call_sign = file.replace(full_path,"")[:-3].replace("\\", "/") 
                    files = search_strings_in_files(katalon_source+keywords, [call_sign])
                    # print(f">> {call_sign}")
                    if not files:
                        print(f"\tNo keyword\t: {call_sign}")
                    else:
                        has_keyword = True
                
                if has_keyword:
                    cnt_keyword += 1

    # Show stats
    print(f"Covered by Objects: {cnt_object}/{cnt_paths} ({(cnt_object/cnt_paths)*100:.2f}%) ") 
    print(f"Covered by Keywords: {cnt_keyword}/{cnt_paths} ({(cnt_keyword/cnt_paths)*100:.2f}%) ") 

    exit()
if __name__ == "__main__":
    main(sys.argv[1:])

