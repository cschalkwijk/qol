# How to use

## Windows 

### Preparation 
- Install python in a _custom_ folder; default install folder is a PITA
- Run ```c:\python3\python -m pip install psutil```
- Run ```c:\python3\python -m pip install requests```
- Run ```c:\python3\python -m pip install rich``

### Running
You need a PAT (personal access token) with read rights to the project.

Powershell:
Run ```python .\check_pipeline.py "[URL]" "[PAT]" [AMOUNT OF RELEASES]```
