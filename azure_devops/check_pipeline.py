import requests
import base64
import os
import sys
from datetime import datetime
import rich

from rich.tree import Tree
from rich.console import Console

console = Console()
# requests module logs extensively to stdErr, even when exceptions
# are caught & handled. To hide these, there seems to be no other
# way than to pipe the output to a file
sys.stderr = open('err.log', 'a')


def excepthook(exc_type, exc_value, exc_traceback):
    with open("err.log", "a") as file:
        timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        file.write(f"[{timestamp}] {exc_type.__name__}")
        file.write(f"ExMessage: {exc_value}\n")
        traceback_text = "".join(traceback.format_tb(exc_traceback))
        file.write(traceback_text)


def check_website_reachable(url):
    try:
        response = requests.get(url, verify=False, timeout=5)
        console.print(f"The website '{url}' is reachable.")
    except Exception as e:
        console.print(f"Failed to reach the website '{url}'")
        return False

    return True


def auth_header():
    return {
        'Authorization': 'Basic ' + base64.b64encode(bytes(':' + pat, 'ascii')).decode('ascii')
    }


def latest_release(url, pipeline_id, cnt=1):
    releases = []
    release_url = f"{url}/_apis/release/releases?api-version=6.0&$top={cnt}&$filter=definitionId eq {pipeline_id}&$orderby=createdOn desc"
    response = requests.get(release_url, verify=False, headers=auth_header(), timeout=5)
    response.raise_for_status()
    release_data = response.json()

    if 'value' in release_data:
        for release in release_data['value']:
            releases.append(release)

    return releases


def check_pipeline_step(url, pipeline_id, release_id, step_name, leaf):
    release_url = f"{url}/_apis/release/releases/{release_id}?api-version=6.0"
    response = requests.get(release_url, verify=False, headers=auth_header())
    release_data = response.json()

    # Find the specified step and check its status
    step_status = None
    for environment in release_data['environments']:
        if environment['name'] == step_name:
            step_status = environment['status']

            col, emoji = ("green", "") if step_status=="succeeded" else ("red blink", ":skull:")

            if step_status is not None:
                leaf.add(f"[orange3]{step_name}[/] > {emoji} [white on {col} bold]{step_status}")
            else:
                leaf.add(f"'{step_name}'\t:not found")


if len(sys.argv) < 4:
    console.print("Usage: python script.py <URL> <PAT> <PIPELINE_ID> <CNT>")
    sys.exit(1)

url = sys.argv[1]
pat = sys.argv[2]
pid = sys.argv[3]
cnt = sys.argv[4]

requests.packages.urllib3.disable_warnings()

# Check Azure Devops availability:
if not check_website_reachable(url):
    console.print("Azure Devops [red]cannot[/] be reached (turn on VPN?)")

releases = latest_release(url, pid, cnt)

date_format = "%Y-%m-%dT%H:%M:%S.%fZ"
current_date = datetime.now().date()
tree = Tree("[white on blue]Releases")

with console.status("[bold green] Reading...") as status:
    for release in releases:
        # Parse the string into a datetime object
        release_date = datetime.strptime(release['createdOn'], date_format)
        style = "[bold yellow]" if current_date == release_date.date() else ""
        
        leaf = tree.add(f"[green]{release['name']} ({release['id']})")

        check_pipeline_step(url, pid, release['id'], "API tests", leaf)

        description = release['description']

        if not description:
            description = f"{release['reason']} by [italic]{release['createdBy']['displayName']}"

        leaf.add(f"{style}{release_date}")
        leaf.add(description)

console.print()
console.print(tree)
console.print()
